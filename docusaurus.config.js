// @ts-check
// Note: type annotations allow type checking and IDEs autocompletion

const lightCodeTheme = require("prism-react-renderer/themes/github");
const darkCodeTheme = require("prism-react-renderer/themes/dracula");

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: "Opt'imo",
  tagline: "Les apis de l'application d'inventaire.",
  url: "https://optimo.fintechgodwin.com",
  baseUrl: "/",
  onBrokenLinks: "throw",
  onBrokenMarkdownLinks: "warn",
  favicon: "img/favicon.ico",
  organizationName: "Opt'imo", // Usually your GitHub org/user name.
  projectName: "Opt'imo", // Usually your repo name.

  /* i18n: {
    defaultLocale: 'en',
    locales: ['en', 'fa'],
    path: 'i18n',
    localeConfigs: {
      en: {
        label: 'English',
        direction: 'ltr',
        htmlLang: 'en-US',
        calendar: 'gregory',
        path: 'en',
      },
      fa: {
        label: 'فارسی',
        direction: 'rtl',
        htmlLang: 'fa-IR',
        calendar: 'persian',
        path: 'fa',
      },
    },
  }, */
  
  presets: [
    [
      "docusaurus-preset-openapi",
      /** @type {import('docusaurus-preset-openapi').Options} */
      ({
        /* docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // Please change this to your repo.
          editUrl:
            "https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/",
        },
        
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            "https://github.com/facebook/docusaurus/tree/main/packages/create-docusaurus/templates/shared/",
        }, */
        docs: {
          path: 'client',
          // other docs options
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
        
      }),
    ],
  ],

  themeConfig:
    /** @type {import('docusaurus-preset-openapi').ThemeConfig} */
    ({
      navbar: {
        title: "Opt'imo",
        logo: {
          alt: "Opt'imo Logo",
          src: "img/logo-white.jpeg",
        },
        items: [
          { to: "/api", label: "Documentations", position: "left" },
          {
            href: "https://gitlab.com/AchoBestman/optimo.git",
            label: "GitLab",
            position: "right",
          },
        ],
      },
      footer: {
        style: "dark",
        links: [
          {
            title: "Resources",
            items: [
              {
                label: "api documentation",
                to: "/api",
              },
            ],
          },
          {
            title: "Community",
            items: [
              {
                label: "LinkeDin",
                href: "https://www.linkedin.com/in/achile-aikpe-018654196/",
              },
            ],
          },
          {
            title: "More",
            items: [
              {
                label: "GitLab",
                href: "https://gitlab.com/AchoBestman/optimo.git",
              }
            ],
          },
        ],
        copyright: `Copyright © ${new Date().getFullYear()} Opt'imo, Inc. All rights reserved.`,
      },
      prism: {
        theme: lightCodeTheme,
        darkTheme: darkCodeTheme,
      },
    }),
};

module.exports = config;
