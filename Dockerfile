# Utilisez une image de base avec Node.js
FROM node:16.14.2

#application volume
VOLUME /tmp

# Set the working directory in the container
WORKDIR /app/omnivox-docs

# Copiez les fichiers de votre application dans le conteneur
COPY package*.json ./
COPY . .

# Installez les dépendances
RUN npm install

# Construisez l'application Next.js
#RUN npm run build

# Expose port 4000 to the host machine

EXPOSE 4000

# Start the Spring Boot application when the container starts
CMD ["npm", "run", "start"]
